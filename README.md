# Examples

## [SOLID](./design/solid)

## [Builder](./design/builder)

## [Factory](./design/factory)

## [Prototype](./design/prototype)

## [Singleton](./design/singleton)

## [Adapter](./design/adapter)

## [Bridge](./design/bridge)

## [Composite](./design/composite)

## [Decorator](./design/decorator)

## [Facade](./design/facade)

## [Flyweight](./design/flyweight)

## [Proxy](./design/proxy)

## [Chain of Responsibility](./design/chainOfResponsibility)

## [Command](./design/command)

## [Interpreter](./design/interpreter)

## [Iterator](./design/iterator)

## [Mediator](./design/mediator)

## [Memento](./design/memento)

## [Observer](./design/observer)

## [State](./design/state)

## [Strategy](./design/strategy)

## [Template](./design/template)

## [Visitor](./design/visitor)
