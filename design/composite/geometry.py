from typing import List

class StringBuffer:
	def __init__(self) -> None:
		self.__result = ""
	
	def append(self, data: str):
		self.__result = self.__result + data

	def toString(self) -> str:
		return self.__result

	def __str__(self) -> str:
		return self.toString()

class GraphicObject:
	def __init__(self, name: str, color: str = None, children: List['GraphicObject'] = None) -> None:
		self.name = name
		self.color = color
		self.children = [] if children is None else children

	def toString(self) -> str:
		result = StringBuffer()
		self.__print(depth=0, result=result)
		return result.toString()

	def __print(self, depth: int, result: StringBuffer):
		result.append("*" * depth)

		if self.color:
			result.append(self.color + " ")

		result.append(self.name + "\n")

		for child in self.children:
			child.__print(depth=depth + 1, result=result)


def newCircle(color: str) -> GraphicObject:
	return GraphicObject(name="Circle", color=color)

def newSquare(color: str) -> GraphicObject:
	return GraphicObject(name="Square", color=color)

def main():
	drawing = GraphicObject(name="My Drawing")
	drawing.children.append(newSquare(color="Red"))
	drawing.children.append(newCircle(color="Yellow"))

	group = GraphicObject("Group 1")
	group.children.append(newCircle("Blue"))
	group.children.append(newSquare("Blue"))

	drawing.children.append(group)

	print(drawing.toString())


main()
