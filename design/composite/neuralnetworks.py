from typing import List


class NeuronInterface:
	def Iter() -> List['Neuron']:
		pass

class Neuron(NeuronInterface):
	def __init__(self) -> None:
		self.In = []
		self.Out = []

	def Iter(self) -> List['Neuron']:
		return [self]

	def ConnectTo(self, other: 'Neuron'):
		self.Out.append(other)
		other.In.append(self)


class NeuronLayer(NeuronInterface):
	def __init__(self, Neurons: List[Neuron]) -> None:
		self.Neurons = Neurons

	def Iter(self) -> List['Neuron']:
		result = []
		for n in self.Neurons:
			result.append(n)
		return result

def NewNeuronLayer() -> NeuronLayer:
	return NeuronLayer([])

def Connect(left: NeuronInterface, right: NeuronInterface):
	for l in left.Iter():
		for r in right.Iter():
			l.ConnectTo(r)

def main():
	neuron1, neuron2 = Neuron(), Neuron()
	layer1, layer2 = NewNeuronLayer(), NewNeuronLayer()

	# neuron1.ConnectTo(&neuron2)

	Connect(neuron1, neuron2)
	Connect(neuron1, layer1)
	Connect(layer2, neuron1)
	Connect(layer1, layer2)

main()
