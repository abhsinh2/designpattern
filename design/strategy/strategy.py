from abc import ABC, abstractmethod
from enum import Enum
from typing import List


class OutputFormat(Enum):
	Markdown = 1
	Html = 2

class StringBuilder:
    def __init__(self) -> None:
        self.__str = ""

    def append(self, data):
        self.__str += data

    def __str__(self) -> str:
        return self.__str

    def reset(self): 
        self.__str = ""

class ListStrategy(ABC):
    @abstractmethod
    def Start(self, builder: StringBuilder):
        pass
        
    @abstractmethod
    def End(self, builder: StringBuilder):
        pass
        
    @abstractmethod
    def AddListItem(self, builder: StringBuilder, item: str):
        pass

class MarkdownListStrategy(ListStrategy):
    def Start(self, builder: StringBuilder):
        pass

    def End(self, builder: StringBuilder):
        pass

    def AddListItem(self, builder: StringBuilder, item: str):
        builder.append(" * " + item + "\n")

class HtmlListStrategy(ListStrategy):
    def Start(self, builder: StringBuilder):
        builder.append("<ul>\n")

    def End(self, builder: StringBuilder):
        builder.append("</ul>\n")

    def AddListItem(self, builder: StringBuilder, item: str):
        builder.append("  <li>" + item + "</li>\n")

class TextProcessor:
    def __init__(self, listStrategy: ListStrategy) -> None:
        self.listStrategy = listStrategy
        self.builder = StringBuilder()

    def SetOutputFormat(self, fmt: OutputFormat):
        if fmt == OutputFormat.Markdown:
            self.listStrategy = MarkdownListStrategy()
        elif fmt == OutputFormat.Html:
            self.listStrategy = HtmlListStrategy()

    def AppendList(self, items: List[str]):
        self.listStrategy.Start(self.builder)
        for item in items:
            self.listStrategy.AddListItem(self.builder, item)
        self.listStrategy.End(self.builder)

    def Reset(self):
        self.builder.reset()

    def __str__(self) -> str:
        return str(self.builder)

def main():
	tp = TextProcessor(MarkdownListStrategy())
	tp.AppendList(["foo", "bar", "baz"])
	print(tp)

	tp.Reset()
	tp.SetOutputFormat(OutputFormat.Html)
	tp.AppendList(["foo", "bar", "baz"])
	print(tp)

main()
