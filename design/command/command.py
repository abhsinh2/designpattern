from abc import ABC, abstractmethod
from enum import Enum

overdraftLimit = -500

class BankAccount:
    def __init__(self) -> None:
        self.balance = 0

    def Deposit(self, amount: int):
        self.balance += amount
        print("Deposited", amount, "\b, balance is now", self.balance)

    def Withdraw(self, amount: int) -> bool:
        if self.balance-amount >= overdraftLimit:
            self.balance -= amount
            print("Withdrew", amount, "\b, balance is now", self.balance)
            return True
        return False

class Command(ABC):
    @abstractmethod
    def Call(self):
        pass
        
    @abstractmethod
    def Undo(self):
        pass

class Action(Enum):
    Deposit = 1
    Withdraw = 2

class BankAccountCommand(Command):
    def __init__(self, account: BankAccount, action: Action, amount: int) -> None:
        self.account = account
        self.action = action
        self.amount = amount
        self.succeeded = False

    def Call(self):
        if self.action == Action.Deposit:
            self.account.Deposit(self.amount)
            self.succeeded = True
        elif self.action == Action.Withdraw:
            self.succeeded = self.account.Withdraw(self.amount)

    def Undo(self):
        if self.succeeded is False:
            return

        if self.action == Action.Deposit:
            self.account.Withdraw(self.amount)
        elif self.action == Action.Withdraw:
            self.account.Deposit(self.amount)

def main():
	ba = BankAccount()
	cmd = BankAccountCommand(ba, Action.Deposit, 100)
	cmd.Call()
	cmd2 = BankAccountCommand(ba, Action.Withdraw, 50)
	cmd2.Call()


main()
