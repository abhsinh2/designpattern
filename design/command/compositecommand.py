from abc import ABC, abstractmethod
from enum import Enum

overdraftLimit = -500

class BankAccount:
    def __init__(self, balance: int = 0) -> None:
        self.balance = balance

    def Deposit(self, amount: int):
        self.balance += amount
        print("Deposited", amount, "\b, balance is now", self.balance)

    def Withdraw(self, amount: int) -> bool:
        if self.balance - amount >= overdraftLimit:
            self.balance -= amount
            print("Withdrew", amount, "\b, balance is now", self.balance)
            return True
        return False

    def __str__(self) -> str:
        return "{" + str(self.balance) + "}"

class Command(ABC):
    @abstractmethod
    def Call(self):
        pass
        
    @abstractmethod
    def Undo(self):
        pass

    @abstractmethod
    def Succeeded(self) -> bool:
        pass

    @abstractmethod
    def SetSucceeded(self, value: bool):
        pass

class Action(Enum):
    Deposit = 1
    Withdraw = 2

class BankAccountCommand(Command):
    def __init__(self, account: BankAccount, action: Action, amount: int) -> None:
        self.account = account
        self.action = action
        self.amount = amount
        self.succeeded = False

    def Call(self):
        if self.action == Action.Deposit:
            self.account.Deposit(self.amount)
            self.succeeded = True
        elif self.action == Action.Withdraw:
            self.succeeded = self.account.Withdraw(self.amount)

    def Undo(self):
        if self.succeeded is False:
            return

        if self.action == Action.Deposit:
            self.account.Withdraw(self.amount)
        elif self.action == Action.Withdraw:
            self.account.Deposit(self.amount) 

    def SetSucceeded(self, value: bool):
        self.succeeded = value

    def Succeeded(self) -> bool:
        return self.succeeded

class CompositeBankAccountCommand(Command):
    def __init__(self) -> None:
        self.commands = []

    def Succeeded(self) -> bool:
        for cmd in self.commands:
            if cmd.Succeeded() is False:
                return False
        return True

    def SetSucceeded(self, value: bool):
        for cmd in self.commands:
            cmd.SetSucceeded(value)

    def Call(self):
        for cmd in self.commands:
            cmd.Call()

    def Undo(self):
        for idx in range(len(self.commands)):
            self.commands[len(self.commands) - idx - 1].Undo()

class MoneyTransferCommand(CompositeBankAccountCommand):
    def __init__(self, From: BankAccount, To: BankAccount, amount: int) -> None:
        super().__init__()
        self.From = From
        self.To = To
        self.amount = amount

    def Call(self):
        ok = True
        for cmd in self.commands:
            if ok:
                cmd.Call()
                ok = cmd.Succeeded()
            else:
                cmd.SetSucceeded(False)

def NewMoneyTransferCommand(From: BankAccount, To: BankAccount, amount: int) -> MoneyTransferCommand:
	c = MoneyTransferCommand(From, To, amount)
	c.commands.append(BankAccountCommand(From, Action.Withdraw, amount))
	c.commands.append(BankAccountCommand(To, Action.Deposit, amount))
	return c

def main():
	ba = BankAccount()
	cmdDeposit = BankAccountCommand(ba, Action.Deposit, 100)
	cmdWithdraw = BankAccountCommand(ba, Action.Withdraw, 1000)
	cmdDeposit.Call()
	cmdWithdraw.Call()

	print(ba)
	cmdWithdraw.Undo()
	cmdDeposit.Undo()
	print(ba)

	bfrom = BankAccount(100)
	bto = BankAccount(0)
	mtc = NewMoneyTransferCommand(bfrom, bto, 100)
	mtc.Call()

	print("from=", bfrom, "to=", bto)

	print("Undoing...")
	mtc.Undo()
	print("from=", bfrom, "to=", bto)

main()
