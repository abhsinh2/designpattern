class BankAccount:
    def __init__(self) -> None:
        self.Balance = 0

def Deposit(ba: BankAccount, amount: int):
    print("Depositing", amount)
    ba.Balance += amount

def Withdraw(ba: BankAccount, amount: int):
    if ba.Balance >= amount:
        print("Withdrawing", amount)
        ba.Balance -= amount

def main():
	ba = BankAccount()

	commands = []
	commands.append(lambda ba: Deposit(ba, 100))
	commands.append(lambda ba: Withdraw(ba, 100))

	for cmd in commands:
		cmd(ba)

main()
