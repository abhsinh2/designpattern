import json

class Email:
    def __init__(self, frm: str = None, to: str = None, subject: str = None, body: str = None) -> None:
        self.frm = frm
        self.to = to
        self.subject = subject
        self.body = body

    def __str__(self) -> str:
        return json.dumps({
            'from': self.frm,
            'to': self.to,
            'subject': self.subject,
            'body': self.body
        })

class EmailBuilder:
    def __init__(self) -> None:
        self.email = Email()

    def frm(self, frm: str) -> 'EmailBuilder':
        self.email.frm = frm
        return self

    def to(self, to: str) -> 'EmailBuilder':
        self.email.to = to
        return self

    def subject(self, subject: str) -> 'EmailBuilder':
        self.email.subject = subject
        return self

    def body(self, body: str) -> 'EmailBuilder':
        self.email.body = body
        return self


def sendMailImpl(email: Email):
	# actually ends the email
    print("Sending email {}".format(email))

def SendEmail(func):
	builder = EmailBuilder()
	func(builder)
	sendMailImpl(builder.email)

def main():
    def func(b: EmailBuilder):
        b.frm("foo@bar.com").to("bar@baz.com").subject("Meeting").body("Hello, do you want to meet?")
        
    SendEmail(func)

if __name__=="__main__":
    main()
