import json

class Person:
    def __init__(self, 
                streetAddress: str = None, 
                postcode: str = None, 
                city: str = None, 
                companyName: str = None, 
                position: str = None, 
                annualIncome: int = None) -> None:
        self.streetAddress = streetAddress
        self.postcode = postcode
        self.city = city
        self.companyName = companyName
        self.position = position
        self.annualIncome = annualIncome

    def __str__(self) -> str:
        dt = {
            'streetAddress': self.streetAddress,
            'postcode': self.postcode,
            'city': self.city,
            'companyName': self.companyName,
            'position': self.position,
            'annualIncome': self.annualIncome
        }
        return json.dumps(dt)

class PersonBuilder:
    def __init__(self) -> None:
        self.person = Person()

    def build(self) -> Person:
        return self.person

    def works(self) -> 'PersonJobBuilder':
        return PersonJobBuilder(self)

    def lives(self) -> 'PersonAddressBuilder':
        return PersonAddressBuilder(self)

class PersonJobBuilder(PersonBuilder):
    def __init__(self, pb: PersonBuilder) -> None:
        super().__init__()
        self.person = pb.person

    def at(self, companyName: str) -> 'PersonJobBuilder':
        self.person.companyName = companyName
        return self

    def asA(self, position: str) -> 'PersonJobBuilder':
        self.person.position = position
        return self

    def earning(self, annualIncome: int) -> 'PersonJobBuilder':
        self.person.annualIncome = annualIncome
        return self

class PersonAddressBuilder(PersonBuilder):
    def __init__(self, pb: PersonBuilder) -> None:
        super().__init__()	
        self.person = pb.person

    def at(self, streetAddress: str) -> 'PersonAddressBuilder':
        self.person.streetAddress = streetAddress
        return self

    def cin(self, city: str) -> 'PersonAddressBuilder':
        self.person.city = city
        return self

    def withPostcode(self, postcode: str) -> 'PersonAddressBuilder':
        self.person.postcode = postcode
        return self

def main():
	pb = PersonBuilder()
	pb.lives().at(
        "123 London Road"
    ).cin(
        "London"
    ).withPostcode(
        "SW12BC"
    ).works().at(
        "Fabrikam"
    ).asA(
        "Programmer"
    ).earning(
        123000
    )
	person = pb.build()
	print(person)


if __name__=="__main__":
    main()
