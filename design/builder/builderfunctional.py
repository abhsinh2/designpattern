import json

class Person:
    def __init__(self, name: str = None, position: str = None) -> None:
        self.name = name
        self.position = position

    def __str__(self) -> str:
        return json.dumps({
            'name': self.name,
            'position': self.position
        })

class PersonBuilder:
    def __init__(self) -> None:
        self.actions = []

    def called(self, name: str) -> 'PersonBuilder':
        def func(p: Person):
            p.name = name
        self.actions.append(func)
        return self

    def build(self) -> Person:
        p = Person()
        for action in self.actions:
            action(p)
        return p

    # extend PersonBuilder
    def worksAsA(self, position: str) -> 'PersonBuilder':
        def func(p: Person):
            p.position = position
        self.actions.append(func)
        return self

def main():
	b = PersonBuilder()
	p = b.called("Dmitri").worksAsA("dev").build()
	print(p)

if __name__=="__main__":
    main()
