from typing import List


class Person:
    def __init__(self, FirstName: str, MiddleName: str, LastName: str):
        self.FirstName = FirstName
        self.MiddleName = MiddleName
        self.LastName = LastName

    def Names(self) -> List[str]:
        return [self.FirstName, self.MiddleName, self.LastName]

def main():
	p = Person("Alexander", "Graham", "Bell")
	for name in p.Names():
		print(name)

main()
