class Node:
    def __init__(self, Value: int, left: 'Node' = None, right: 'Node' = None) -> None:
        self.Value = Value
        self.left = left
        self.right = right
        self.parent = None

        if self.left:
            self.left.parent = self
        if self.right:
            self.right.parent = self

class InOrderIterator:
    def __init__(self, root: Node,) -> None:
        self.root = root
        self.Current = root
        self.returnedStart = False

        # move to the leftmost element
        while self.Current.left:
            self.Current = self.Current.left

    def Reset(self): 
        self.Current = self.root
        self.returnedStart = False

    def MoveNext(self) -> bool:
        if self.Current is None:
            return False
        
        if self.returnedStart is False:
            self.returnedStart = True
            return True # can use first element

        if self.Current.right is not None:
            self.Current = self.Current.right
            while self.Current.left is not None:
                self.Current = self.Current.left
            return True
        else:
            p = self.Current.parent
            while p is not None and self.Current == p.right:
                self.Current = p
                p = p.parent
            self.Current = p
            return self.Current is not None

class BinaryTree:
    def __init__(self, root: Node) -> None:
        self.root = root

    def InOrder(self) -> InOrderIterator:
        return InOrderIterator(self.root)

def main():    
    root = Node(1, Node(2), Node(3))
    it = InOrderIterator(root)
    
    while it.MoveNext():
        print("{}".format(it.Current.Value))

    print()	
    
    t = BinaryTree(root)
    i = t.InOrder()
    while i.MoveNext():
        print("{}".format(i.Current.Value))
            
main()
