class Renderer:
    def renderCircle(self, radius: float):
        pass


class VectorRenderer(Renderer):
    def renderCircle(self, radius: float):
        print("Drawing a circle of radius", radius)


class RasterRenderer(Renderer):
    def __init__(self, dpi: int) -> None:
        super().__init__()
        self.dpi = dpi

    def renderCircle(self, radius: float):
        print("Drawing pixels for circle of radius", radius)


class Circle:
    def __init__(self, renderer: Renderer, radius: float) -> None:
        self.renderer = renderer
        self.radius = radius
	
    def draw(self):
        self.renderer.renderCircle(self.radius)

    def resize(self, factor: float):
        self.radius *= factor

def main():
	raster = RasterRenderer(1)
	vector = VectorRenderer()
	circle = Circle(renderer=vector, radius=5)
	circle.draw()

main()
