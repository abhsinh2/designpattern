1. Bridge prevents a 'Cartesian product' complexity explosion.

2. Bridge pattern avoids entity explosion.

Before:

```
                                                     ThreadScheduler
            PreemptiveThreadScheduler (PTS)                                        CooperativeThreadScheduler (CTS)
                WindowPTS  UnixPTS                                                       WindowCTS UnixCTS
```

After

```
ThreadScheduler
    - platformScheduler

PreemptiveThreadScheduler                               IPlatformScheduler
CooperativeThreadScheduler                        UnixScheduler    WindowScheduler   
```

3. A mechanism the decouples an interface (hierarchy) from an implementation (hierarchy)

4. Decouple abstraction from implementation

5. Both can exist as hierarchies

6. A stronger form of encapsulation
