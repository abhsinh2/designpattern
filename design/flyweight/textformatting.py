from typing import List

class StringBuilder:
    def __init__(self) -> None:
        self.__str = ""

    def append(self, data: str):
        self.__str = self.__str + data

    def String(self) -> str:
        return self.__str


class FormattedText:
    def __init__(self, plainText: str) -> None:
        self.plainText = plainText
        self.capitalize = [False for _ in range(len(self.plainText))]

    def String(self) -> str:
        sb = StringBuilder()
        for i in range(len(self.plainText)):
            c = self.plainText[i]
            if self.capitalize[i]:
                sb.append(c.upper())
            else:
                sb.append(c)
        return sb.String()

    def Capitalize(self, start: int, end: int):
        for i in range(start, end + 1):
            self.capitalize[i] = True

class TextRange:
    def __init__(self, Start: int, End: int, Capitalize: bool, Bold: bool, Italic: bool) -> None:
        self.Start = Start
        self.End = End
        self.Capitalize = Capitalize
        self.Bold = Bold
        self.Italic = Italic
	
    def Covers(self, position: int) -> bool:
        return position >= self.Start and position <= self.End


class BetterFormattedText:
    def __init__(self, plainText: str) -> None:
        self.plainText = plainText
        self.formatting = []

    def String(self) -> str:
        sb = StringBuilder()
        for i in range(len(self.plainText)):
            c = self.plainText[i]
            for r in self.formatting:
                if r.Covers(i) and r.Capitalize:
                    c = c.upper()
            sb.append(c)
        return sb.String()

    def Range(self, start: int, end: int) -> TextRange:
        r = TextRange(start, end, False, False, False)
        self.formatting.append(r)
        return r

def main():
	text = "This is a brave new world"

	ft = FormattedText(text)
	ft.Capitalize(10, 15) # brave
	print(ft.String())

	bft = BetterFormattedText(text)
	bft.Range(16, 19).Capitalize = True # new
	print(bft.String())

main()
