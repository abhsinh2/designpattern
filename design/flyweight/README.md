1. Space optimization

2. Avoid redundancy when storing data.

3. Example: bold and italic formatting. Don't want each character to have a formatting chartacter. Rather operate on ranges like line numbers / start-end position.

4. A space optimization technique that let us use less memory by storing externally the data associated with similar objects.
