from typing import List


class User:
    def __init__(self, FullName: str) -> None:
        self.FullName = FullName

allNames = []

class User2:
    def __init__(self) -> None:
        self.names = []

    def FullName(self) -> str:
        parts = []
        for n in self.names:
            parts.append(allNames[n])
        return " ".join(parts)

def NewUser2(fullName: str) -> User2:
	def getOrAdd(s: str) -> int:
		for i in range(len(allNames)):
			if allNames[i] == s:
				return int(i)
		allNames.append(s)
		return len(allNames) - 1

	result = User2()
	parts = fullName.split(" ")
	for p in parts:
		result.names.append(getOrAdd(p))
	return result

def main():
	john = User("John Doe")
	jane = User("Jane Doe")
	alsoJane = User("Jane Smith")
	print(john.FullName)
	print(jane.FullName)
	print("Memory taken by users:", len(john.FullName) + len(alsoJane.FullName)+ len(jane.FullName))

	john2 = NewUser2("John Doe")
	jane2 = NewUser2("Jane Doe")
	alsoJane2 = NewUser2("Jane Smith")
	print(john2.FullName())
	print(jane2.FullName())
	totalMem = 0
	for a in allNames:
		totalMem += len(a)
	totalMem += len(john2.names)
	totalMem += len(jane2.names)
	totalMem += len(alsoJane2.names)
	print("Memory taken by users2 {}".format(totalMem))

main()
