from abc import ABC, abstractmethod


class Creature:
    def __init__(self, Name: str, Attack: int, Defense: int) -> None:
        self.Name = Name
        self.Attack = Attack
        self.Defense = Defense

    def String(self) -> str:
        return "{} ({}/{})".format(self.Name, self.Attack, self.Defense)

class Modifier(ABC):
    @abstractmethod
    def Add(self, m: 'Modifier'):
        pass
        
    @abstractmethod
    def Handle(self):
        pass

class CreatureModifier:
    def __init__(self, creature: Creature) -> None:
        self.creature = creature
        self.next = None

    def Add(self, m: Modifier):
        if self.next is not None:
            self.next.Add(m)
        else:
            self.next = m

    def Handle(self):
        if self.next is not None:
            self.next.Handle()

class DoubleAttackModifier(CreatureModifier):
    def __init__(self, creature: Creature) -> None:
        super().__init__(creature)

    def Handle(self):
        print("Doubling", self.creature.Name, "attack...")
        self.creature.Attack *= 2
        super().Handle()

class IncreasedDefenseModifier(CreatureModifier):
    def __init__(self, creature: Creature) -> None:
        super().__init__(creature)

    def Handle(self):
        if self.creature.Attack <= 2:
            print("Increasing", self.creature.Name, "\b's defense")
            self.creature.Defense += 1
        super().Handle()

class NoBonusesModifier(CreatureModifier):
    def __init__(self, creature: Creature) -> None:
        super().__init__(creature)

    def Handle(self):
        pass

def main():
	goblin = Creature("Goblin", 1, 1)
	print(goblin.String())

	root = CreatureModifier(goblin)

	# root.Add(NoBonusesModifier(goblin))

	root.Add(DoubleAttackModifier(goblin))
	root.Add(IncreasedDefenseModifier(goblin))
	root.Add(DoubleAttackModifier(goblin))

	# eventually process the entire chain
	root.Handle()
	print(goblin.String())

main()
