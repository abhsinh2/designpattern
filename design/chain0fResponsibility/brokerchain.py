from abc import ABC, abstractmethod
from enum import Enum

class Argument(Enum):
	Attack = 1
	Defense = 2

class Query:
	def __init__(self, CreatureName: str, WhatToQuery: Argument, Value: int) -> None:
		self.CreatureName = CreatureName
		self.WhatToQuery = WhatToQuery
		self.Value = Value

class Observer(ABC):
	@abstractmethod
	def Handle(self, query: Query):
		pass

class Observable(ABC):
	@abstractmethod
	def Subscribe(self, o: Observer):
		pass

	@abstractmethod
	def Unsubscribe(self, o: Observer):
		pass

	@abstractmethod
	def Fire(self, q: Query):
		pass

class Game:
	def __init__(self) -> None:
		self.observers = dict()

	def Subscribe(self, o: Observer):
		self.observers[o] = {}

	def Unsubscribe(self, o: Observer):
		del self.observers[o]

	def Fire(self, q: Query):
		for o in self.observers:
			o.Handle(q)

class Creature:
	def __init__(self, game: Game, Name: str, attack: int, defense: int) -> None:
		self.game = game
		self.Name = Name
		self.attack = attack
		self.defense = defense

	def Attack(self) -> int:
		q = Query(self.Name, Argument.Attack, self.attack)
		self.game.Fire(q)
		return q.Value

	def Defense(self) -> int:
		q = Query(self.Name, Argument.Defense, self.defense)
		self.game.Fire(q)
		return q.Value

	def String(self) -> str:
		return "{} ({}/{})".format(self.Name, self.Attack(), self.Defense())

# data common to all modifiers
class CreatureModifier(ABC):
	def __init__(self, game: Game, creature: Creature) -> None:
		self.game = game
		self.creature = creature

	@abstractmethod
	def Handle(self, query: Query):
		pass

class DoubleAttackModifier(CreatureModifier):
	def __init__(self, game: Game, creature: Creature) -> None:
		super().__init__(game, creature)
		game.Subscribe(self)

	def Handle(self, q: Query):
		if q.CreatureName == self.creature.Name and q.WhatToQuery == Argument.Attack:
			q.Value *= 2

	def Close(self):
		self.game.Unsubscribe(self)

def main():
	game = Game()
	goblin = Creature(game, "Strong Goblin", 2, 2)
	print(goblin.String())

	print("------------------")

	m = DoubleAttackModifier(game, goblin)
	print(goblin.String())
	m.Close()

	print("------------------")

	print(goblin.String())

main()
