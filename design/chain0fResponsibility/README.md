1. Sequence of handlers processing an event one after another.

2. Example: Employee -> Manager -> CEO

3. A chain of components who all get a chance to process a command or a query, optionally having default processing implementation and an ability to terminate the processing chain.

