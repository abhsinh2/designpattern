import json

class Employee:
    def __init__(self, name: str, position: str, annualIncome: int) -> None:
        self.name = name
        self.position = position
        self.annualIncome = annualIncome

    def __str__(self) -> str:
        return json.dumps({
            'name': self.name,
            'position': self.position,
            'annualIncome': self.annualIncome
        })
        
	
# what if we want factories for specific roles?

# functional approach
def newEmployeeFactory(position: str, annualIncome: int):
    def func(name: str) -> Employee:
        return Employee(name, position, annualIncome)
    return func


# structural approach
class EmployeeFactory:
    def __init__(self, position: str, annualIncome: int) -> None:
        self.position = position
        self.annualIncome = annualIncome

    def create(self, name: str) -> Employee:
	    return Employee(name, self.position, self.annualIncome)


def NewEmployeeFactory2(position: str, annualIncome: int) -> EmployeeFactory:
	return EmployeeFactory(position, annualIncome)

def main():
	developerFactory = newEmployeeFactory("Developer", 60000)
	managerFactory = newEmployeeFactory("Manager", 80000)

	developer = developerFactory("Adam")
	print(developer)

	manager = managerFactory("Jane")
	print(manager)

	bossFactory = NewEmployeeFactory2("CEO", 100000)
	# can modify post-hoc
	bossFactory.annualIncome = 110000
	boss = bossFactory.create("Sam")
	print(boss)

main()
