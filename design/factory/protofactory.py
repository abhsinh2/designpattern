import json
from enum import Enum

class Employee:
    def __init__(self, name: str, position: str, annualIncome: int) -> None:
        self.name = name
        self.position = position
        self.annualIncome = annualIncome

    def __str__(self) -> str:
        return json.dumps({
            'name': self.name,
            'position': self.position,
            'annualIncome': self.annualIncome
        })

class Role(Enum):
    Developer = 0
    Manager = 1

# functional
def newEmployee(role: Role) -> Employee:
    if role == Role.Developer:
        return Employee("", "Developer", 60000)
    elif role == Role.Manager:
        return Employee("", "Manager", 80000)
    else:
        raise Exception("unsupported role")

def main():
	m = newEmployee(Role.Manager)
	m.name = "Sam"
	print(m)

main()
