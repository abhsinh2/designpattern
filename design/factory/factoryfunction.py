import json

class Person:
    def __init__(self, name: str, age: int) -> None:
        self.name = name
        self.age = age

    def __str__(self) -> str:
        return json.dumps({
            'name': self.name,
            'age': self.age
        })

def newPerson(name: str, age: int) -> Person:
	return Person(name, age)


def main():
	# initialize directly
	p = Person("John", 22)
	print(p)

	# use a constructor
	p2 = newPerson("Jane", 21)
	p2.Age = 30
	print(p2)

main()
