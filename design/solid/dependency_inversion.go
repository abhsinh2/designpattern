package main

import "fmt"

// Dependency Inversion Principle
// High Level Module should not depend on Low Level Module
// Both should depend on abstractions

type Relationship int

const (
	Parent Relationship = iota
	Child
	Sibling
)

type Person struct {
	name string
	// other useful stuff here
}

type Info struct {
	from         *Person
	relationship Relationship
	to           *Person
}

// Low level module
type Relationships struct {
	relations []Info
}

func (rs *Relationships) AddParentAndChild(parent, child *Person) {
	rs.relations = append(rs.relations,
		Info{parent, Parent, child})
	rs.relations = append(rs.relations,
		Info{child, Child, parent})
}

// High Level Module
type Research struct {
	// break Dependency inversion
	relationships Relationships // low-level
}

func (r *Research) Investigate() {
	relations := r.relationships.relations
	for _, rel := range relations {
		if rel.from.name == "John" && rel.relationship == Parent {
			fmt.Println("John has a child called", rel.to.name)
		}
	}
}

// Solution
// Both should depend on abstractions 'RelationshipBrowser'
type RelationshipBrowser interface {
	FindAllChildrenOf(name string) []*Person
}

// Implement RelationshipBrowser to low level
func (rs *Relationships) FindAllChildrenOf(name string) []*Person {
	result := make([]*Person, 0)

	for i, v := range rs.relations {
		if v.relationship == Parent && v.from.name == name {
			result = append(result, rs.relations[i].to)
		}
	}

	return result
}

// High Level Module
type Research2 struct {
	// relationships Relationships
	browser RelationshipBrowser // low-level
}

func (r *Research2) Investigate() {
	for _, p := range r.browser.FindAllChildrenOf("John") {
		fmt.Println("John has a child called", p.name)
	}
}

func main() {
	parent := Person{"John"}
	child1 := Person{"Chris"}
	child2 := Person{"Matt"}

	// low-level module
	relationships := Relationships{}
	relationships.AddParentAndChild(&parent, &child1)
	relationships.AddParentAndChild(&parent, &child2)

	research1 := Research{relationships}
	research1.Investigate()

	research := Research2{&relationships}
	research.Investigate()
}
