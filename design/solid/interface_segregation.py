class Document:
    pass

class Machine:
    def print(self, d: Document):
        pass
    
    def fax(self, d: Document):
        pass
    
    def scan(self, d: Document):
        pass


# ok if you need a multifunction device
class MultiFunctionPrinter(Machine):
    def print(self, d: Document):
        pass
    
    def fax(self, d: Document):
        pass
    
    def scan(self, d: Document):
        pass


class OldFashionedPrinter(Machine):
    def print(self, d: Document):
        pass
    
    def fax(self, d: Document):
        raise Exception("operation not supported")
    
    def scan(self, d: Document):
        raise Exception("operation not supported")


# better approach: split into several interfaces

class Printer:
    def print(self, d: Document):
        pass

class Scanner:
    def scan(self, d: Document):
        pass

# printer only
class MyPrinter(Printer):
    def print(self, d: Document):
        pass

# combine interfaces
class Photocopier:
    def print(self, d: Document):
        pass

    def scan(self, d: Document):
        pass

class MultiFunctionDevice(Printer, Scanner):
    pass

# interface combination + decorator
class MultiFunctionMachine:
    def __init__(self, printer: Printer, scanner: Scanner) -> None:
        self.printer = printer
        self.scanner = scanner

    def print(self, d: Document):
        self.printer.print(d)

    def scan(self, d: Document):
        self.scanner.scan(d)
