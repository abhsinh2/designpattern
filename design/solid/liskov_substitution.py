class Sized:
    def getWidth(self) -> int:
        pass
    
    def setWidth(self, width: int):
        pass
    
    def getHeight(self) -> int:
        pass

    def setHeight(self, height: int):
        pass


class Rectangle(Sized):
    def __init__(self, width: int, height: int) -> None:
        self.width = width
        self.height = height

    def getWidth(self) -> int:
        return self.width

    def setWidth(self, width: int):
        self.width = width

    def getHeight(self) -> int:
        return self.height

    def setHeight(self, height: int):
        self.height = height

# modified LSP
# If a function takes an interface and
# works with a type T that implements this
# interface, any structure that aggregates T
# should also be usable in that function.
class Square(Rectangle):
    def __init__(self, size: int) -> None:
        super().__init__(size, size)
        self.size = size

    # Below breaks the priciple as passing Square to UseIt, not gives expceted result
    def setWidth(self, width: int):
        self.width = width
        self.height = width

    def setHeight(self, height: int):
        self.width = height
        self.height = height


# One of the solution
class Square2:
    def __init__(self, size: int) -> None:
	    self.size = size
    
    def rectangle(self) -> Rectangle:
        return Rectangle(self.size, self.size)

def UseIt(sized: Sized):
	width = sized.getWidth()
	sized.setHeight(10)
	expectedArea = 10 * width
	actualArea = sized.getWidth() * sized.getHeight()
	print("Expected an area of {}, but got {}".format(expectedArea, actualArea))

def main():
    rc = Rectangle(2, 3)
    UseIt(rc)
    
    sq = Square(5)
    UseIt(sq)

    sq2 = Square2(5)
    UseIt(sq2.rectangle())

if __name__=="__main__":
    main()
