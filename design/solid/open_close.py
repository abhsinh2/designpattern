from enum import Enum
from typing import List

class Color(Enum):
    red = 0
    green = 1
    blue = 2

class Size(Enum):
    small = 0
    medium = 1
    large = 2

class Product:
    def __init__(self, name: str, color: Color, size: Size) -> None:
        self.name = name
        self.color = color
        self.size = size
    
# Filter outside of Product
class Filter:
    def byColor(self, products: List[Product], color: Color) -> List[Product]:
        return [product for product in products if product.color == color]
    
    def bySize(self, products: List[Product], size: Size) -> List[Product]:
        return [product for product in products if product.size == size]

    def bySizeAndColor(self, products: List[Product], size: Size, color: Color) -> List[Product]:
        return [product for product in products if product.size == size and product.color == color]

# Specification pattern
class Specification:
    def isSatisfied(product: Product) -> bool:
        pass

class ColorSpecification(Specification):
    def __init__(self, color: Color) -> None:
        self.color = color

    def isSatisfied(self, product: Product) -> bool:
        return product.color == self.color

class SizeSpecification(Specification):
    def __init__(self, size: Size) -> None:
        self.size = size

    def isSatisfied(self, product: Product) -> bool:
        return product.size == self.size

class AndSpecification(Specification):
    def __init__(self, first: Specification, second: Specification) -> None:
        self.first = first
        self.second = second

    def isSatisfied(self, product: Product) -> bool:
        return self.first.isSatisfied(product) and self.second.isSatisfied(product)

class BetteFilter:
    def filter(self, products: List[Product], specification: Specification) -> List[Product]:
        return [product for product in products if specification.isSatisfied(product)]

def main():
    apple = Product("Apple", Color.green, Size.small)
    tree = Product("Tree", Color.green, Size.large)
    house = Product("House", Color.blue, Size.large)
    
    products = [apple, tree, house]
    print("Green products (old):")

    f = Filter()
    for product in f.byColor(products, Color.green):
        print(" - {} is green".format(product.name))

	## ^^^ BEFORE

	## AFTER
    print("Green products (new)")
    greenSpec = ColorSpecification(Color.green)
    bf = BetteFilter()
    for product in bf.filter(products, greenSpec):
        print(" - {} is green".format(product.name))
        
    largeSpec = SizeSpecification(Size.large)
    largeGreenSpec = AndSpecification(largeSpec, greenSpec)
    print("Large blue items")
    for product in bf.filter(products, largeGreenSpec):
        print(" - {} is large and green".format(product.name))


if __name__=="__main__":
    main()
