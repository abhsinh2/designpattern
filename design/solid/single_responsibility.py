class Journal:
    def __init__(self) -> None:
        self.entries = []
        self.__entryCount = 0

    def addEntry(self, text: str) -> int:
        self.__entryCount += 1
        entry = "{}: {}".format(self.__entryCount, text)
        self.entries.append(entry)
        return self.__entryCount

    def removeEntry(self):
        pass

    def __str__(self) -> str:
        return '\n'.join(self.entries)
    
    # Break of Single Responsibility Principle

    def save(self, filename: str, lineSeparator: str):
        with open(filename, mode="w") as file:
            file.write(lineSeparator.join(self.entries))

    def load(self, filename):
        pass

    def loadFromWeb():
        pass


# Solution 1:
def saveToFile(journal: Journal, filename: str, lineSeparator: str):
    with open(filename, mode="w") as file:
            file.write(lineSeparator.join(journal.entries))

# Solution 2:
class Persistence:
    def __init__(self, lineSeparator: str) -> None:
        self.lineSeparator = lineSeparator

    def saveToFile(self, journal: Journal, filename: str):
        with open(filename, mode="w") as file:
            file.write(self.lineSeparator.join(journal.entries))


def main():
    j = Journal()
    j.addEntry("I cried today.")
    j.addEntry("I ate a bug")
    print(str(j))

	# separate function
    # saveToFile(j, "journal.txt", "\n")

	# separate class
    p = Persistence("\n")
    p.saveToFile(j, "journal.txt")


if __name__=="__main__":
    main()
