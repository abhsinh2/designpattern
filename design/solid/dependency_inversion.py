# Dependency Inversion Principle
# High Level Module should not depend on Low Level Module
# Both should depend on abstractions

from enum import Enum
from typing import List

class Relationship(Enum):
    Parent = 1
    Child = 2
    Sibling = 3

class Person:
    def __init__(self, name: str) -> None:
        self.name = name

class Info:
    def __init__(self, fromPerson: Person, relationship: Relationship, toPerson: Person) -> None:
        self.fromPerson = fromPerson
        self.relationship = relationship
        self.toPerson = toPerson

# Low level module
class Relationships:
    def __init__(self) -> None:
        self.relations = []

    def addParentAndChild(self, parent: Person, child: Person):
        self.relations.append(Info(parent, Relationship.Parent, child))
        self.relations.append(Info(child, Relationship.Child, parent))


# High Level Module
class Research:
    # break Dependency inversion
    def __init__(self, relationships: Relationships) -> None:
        self.relationships = relationships	

    def investigate(self):
        relations = self.relationships.relations
        for rel in relations:
            if rel.fromPerson.name == "John" and rel.relationship == Relationship.Parent:
                print("John has a child called", rel.toPerson.name)

# Solution
# Both should depend on abstractions 'RelationshipBrowser'
class RelationshipBrowser:
    def findAllChildrenOf(self, name: str) -> List[Person]:
        pass

# Low level module
class Relationships2(RelationshipBrowser):
    def __init__(self) -> None:
        self.relations = []

    def addParentAndChild(self, parent: Person, child: Person):
        self.relations.append(Info(parent, Relationship.Parent, child))
        self.relations.append(Info(child, Relationship.Child, parent))

    def findAllChildrenOf(self, name: str) -> List[Person]:
        return [v.toPerson for v in self.relations if v.relationship == Relationship.Parent and v.fromPerson.name == name]

# High Level Module
class Research2:
    def __init__(self, relationships: Relationships2) -> None:
        self.relationships = relationships

    def investigate(self):
        for p in self.relationships.findAllChildrenOf("John"):
            print("John has a child called", p.name)

def main():
    parent = Person("John")
    child1 = Person("Chris")
    child2 = Person("Matt")

	# low-level module
    relationships = Relationships()
    relationships.addParentAndChild(parent, child1)
    relationships.addParentAndChild(parent, child2)
    
    research1 = Research(relationships)
    research1.investigate()

    relationships2 = Relationships2()
    relationships2.addParentAndChild(parent, child1)
    relationships2.addParentAndChild(parent, child2)
    
    research2 = Research2(relationships2)
    research2.investigate()

if __name__=="__main__":
    main()
