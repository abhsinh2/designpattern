import json

class Address:
    def __init__(self, streetAddress: str, city: str, country: str) -> None:
        self.streetAddress = streetAddress
        self.city = city
        self.country = country
    
    def __str__(self) -> str:
        return json.dumps({
            'streetAddress': self.streetAddress,
            'city': self.city,
            'country': self.country
        })
        
class Person:
    def __init__(self, name: str, address: Address) -> None:
        self.name = name
        self.address = address
        
    def __str__(self) -> str:
        return json.dumps({
            'name': self.name,
            'address': str(self.address)
        })

def main():
	john = Person(name="John", address=Address("123 London Rd", "London", "UK"))

	# jane = john

	# shallow copy
	# jane.Name = "Jane" // ok
	# jane.Address.StreetAddress = "321 Baker St"
	# print(john.name, john.address)
	# print(jane.aame, jane.address)

	# what you really want
	jane = Person(name="Jane", address=Address(john.address.streetAddress, john.address.city, john.address.country))
	jane.address.streetAddress = "321 Baker St"

	print(john.name, john.address)
	print(jane.name, jane.address)


main()
