1. Complicated objects are not designed from scratch. They iterate existing designs.

2. An existing design (partially or fully constructed) design is a prototype.

3. We make a copy of the prototype and customize it. Require deep copy support.

4. Make cloning convenient using Factory
