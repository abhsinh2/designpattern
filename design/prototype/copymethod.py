import json
from typing import List

class Address:
    def __init__(self, streetAddress: str, city: str, country: str) -> None:
        self.streetAddress = streetAddress
        self.city = city
        self.country = country

    def deepCopy(self) -> 'Address':
        return Address(self.streetAddress, self.city, self.country)
    
    def __str__(self) -> str:
        return json.dumps({
            'streetAddress': self.streetAddress,
            'city': self.city,
            'country': self.country
        })

class Person:
    def __init__(self, name: str, address: Address, friends: List[str]) -> None:
        self.name = name
        self.address = address
        self.friends = friends

    def deepCopy(self) -> 'Person':
        return Person(self.name, self.address.deepCopy(), [*self.friends])
        
    def __str__(self) -> str:
        return json.dumps({
            'name': self.name,
            'address': str(self.address),
            'friends': self.friends
        })

def main():
	john = Person("John", Address("123 London Rd", "London", "UK"), ["Chris", "Matt"])

	jane = john.deepCopy()
	jane.name = "Jane"
	jane.address.streetAddress = "321 Baker St"
	jane.friends.append("Angela")

	print(john)
	print(jane)

main()
