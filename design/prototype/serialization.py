import json
from typing import Dict, List, Any
import base64

class Address:
    def __init__(self, streetAddress: str, city: str, country: str) -> None:
        self.streetAddress = streetAddress
        self.city = city
        self.country = country

    def to_json(self) -> Dict[str, str]:
        return {
            'streetAddress': self.streetAddress,
            'city': self.city,
            'country': self.country
        }

    @classmethod
    def from_json(cls, data: Dict[str, str]) -> 'Address':
        return Address(data.get('streetAddress'), data.get('city'), data.get('country'))

    def __str__(self) -> str:
        return json.dumps(self.to_json)

class Person:
    def __init__(self, name: str, address: Address, friends: List[str]) -> None:
        self.name = name
        self.address = address
        self.friends = friends

    def to_json(self) -> Dict[str, Any]:
        return {
            'name': self.name,
            'address': self.address.to_json(),
            'friends': [*self.friends]
        }

    @classmethod
    def from_json(cls, data: Dict[str, Any]) -> 'Person':
        return Person(
            name=data.get('name'), 
            address=Address.from_json(data.get('address')),
            friends=data.get('friends')
        )
        
    def __str__(self) -> str:
        return json.dumps(self.to_json())

    def deepCopy(self) -> 'Person':
        json_data = self.to_json()
        return Person.from_json(json_data)

def main():
	john = Person("John", Address("123 London Rd", "London", "UK"), ["Chris", "Matt", "Sam"])

	jane = john.deepCopy()
	jane.name = "Jane"
	jane.address.streetAddress = "321 Baker St"
	jane.friends.append("Jill")

	print(john)
	print(jane)

main()
