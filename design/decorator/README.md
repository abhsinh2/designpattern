1. Adding behaviour without altering the type itself.

2. Do not want to rewrite and alter existing code (Open - Close principle)

3. Want to keep new functionality separate.

4. Facilitates the addition of behaviours to individual objects through embedding.
 