class Shape:
    def Render() -> str:
        pass

class Circle(Shape):
    def __init__(self, Radius: float) -> None:
        super().__init__()
        self.Radius = Radius

    def Render(self) -> str:
        return "Circle of radius {}".format(self.Radius)

    def Resize(self, factor: float):
        self.Radius *= factor

class Square(Shape):
    def __init__(self, Side: float) -> None:
        super().__init__()
        self.Side = Side

    def Render(self) -> str:
        return "Square with side {}".format(self.Side)

class ColoredShape(Shape):
    def __init__(self, Shape: Shape, Color: str) -> None:
        super().__init__()
        self.Shape = Shape
        self.Color = Color

    def Render(self) -> str:
        return "{} has the color {}".format(self.Shape.Render(), self.Color)

class TransparentShape(Shape):
    def __init__(self, Shape: Shape, Transparency: float) -> None:
        super().__init__()
        self.Shape = Shape
        self.Transparency = Transparency

    def Render(self) -> str:
        return "{} has {} transparency".format(self.Shape.Render(), self.Transparency *100.0 )

def main():
	circle = Circle(2)
	print(circle.Render())

	redCircle = ColoredShape(circle, "Red")
	print(redCircle.Render())

	rhsCircle = TransparentShape(redCircle, 0.5)
	print(rhsCircle.Render())


main()
