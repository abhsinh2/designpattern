class Aged:
    def Age() -> int:
        pass
    
    def SetAge(age: int):
        pass

class Bird(Aged):
    def __init__(self, age: int = 0) -> None:
        super().__init__()
        self.age = age
	
    def Age(self) -> int :
        return self.age
    
    def SetAge(self, age: int):
        self.age = age

    def Fly(self):
        if self.age >= 10:
            print("Flying!")

class Lizard(Aged):
    def __init__(self, age: int = 0) -> None:
        super().__init__()
        self.age = age

    def Age(self) -> int :
        return self.age
    
    def SetAge(self, age: int):
        self.age = age

    def Crawl(self):
        if self.age < 10:
            print("Crawling!")

class Dragon(Aged):
    def __init__(self, bird: Bird = None, lizard: Lizard = None) -> None:
        super().__init__()
        self.bird = Bird() if bird is None else bird
        self.lizard = Lizard() if lizard is None else lizard

    def Age(self) -> int:
        return self.bird.age

    def SetAge(self, age: int):
        self.bird.SetAge(age)
        self.lizard.SetAge(age)

    def Fly(self):
        self.bird.Fly()

    def Crawl(self):
        self.lizard.Crawl()

def main():
    d = Dragon()
    d.bird.age = 10
    print(d.lizard.age)
    d.Fly()
    d.Crawl()
    print("********")
    d1 = Dragon(Bird(), Lizard())
    d1.SetAge(10)
    d1.Fly()
    d1.Crawl()

main()
