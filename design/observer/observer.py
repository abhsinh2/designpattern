class Observable:
    def __init__(self) -> None:
        self.subs = []

    def Subscribe(self, x: 'Observer'):
        self.subs.append(x)

    def Unsubscribe(self, x: 'Observer'):
        for i in self.subs:
            if i == x:
                self.subs.remove(i)

    def Fire(self, data):
        for z in self.subs:
            z.Notify(data)

class Observer:
    def Notify(self, data):
        pass

# whenever a person catches a cold,
# a doctor must be called
class Person(Observable):
    def __init__(self, Name: str) -> None:
        super().__init__()
        self.Name = Name

    def CatchACold(self):
        self.Fire(self.Name)

class DoctorService(Observer):
    def Notify(self, data):
        print("A doctor has been called for {}".format(data))

def main():
	p = Person("Boris")
	ds = DoctorService()
	p.Subscribe(ds)

	# let's test it!
	p.CatchACold()

main()
