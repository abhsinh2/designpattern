class Observable:
    def __init__(self) -> None:
        self.subs = []

    def Subscribe(self, x: 'Observer'):
        self.subs.append(x)

    def Unsubscribe(self, x: 'Observer'):
        for i in self.subs:
            if i == x:
                self.subs.remove(i)

    def Fire(self, data):
        for z in self.subs:
            z.Notify(data)

class Observer:
    def Notify(self, data):
        pass

class Person(Observable):
    def __init__(self, age: int) -> None:
        super().__init__()
        self.__age = age

    @property
    def age(self) -> int:
        return self.__age

    @age.setter
    def age(self, age: int):
        if age == self.__age:
            return
            
        oldCanVote = self.CanVote()
        self.__age = age
        self.Fire(PropertyChanged("age", self.__age))
        
        if oldCanVote != self.CanVote():
            self.Fire(PropertyChanged("CanVote", self.CanVote()))

    def CanVote(self) -> bool:
        return self.age >= 18

class PropertyChanged:
    def __init__(self, Name: str, Value) -> None:
        self.Name = Name
        self.Value = Value

class ElectrocalRoll(Observer):
    def Notify(self, data: PropertyChanged):
        if data.Name == "CanVote" and data.Value:
            print("Congratulations, you can vote!")

def main():
	p = Person(0)
	er = ElectrocalRoll()
	p.Subscribe(er)

	for i in range(10, 20):
		print("Setting age to", i)
		p.age = i
    
main()
