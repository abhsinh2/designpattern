1. We need to be informed when certain things happen.
    - Object field change
    - Object does something
    - some external event occurs

2. We want to listen to events and notified when they occur

3. An observer is an object that wishes to be informed about events happening in the system. The entity generating the events is an observable.

