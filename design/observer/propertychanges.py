class Observable:
    def __init__(self) -> None:
        self.subs = []

    def Subscribe(self, x: 'Observer'):
        self.subs.append(x)

    def Unsubscribe(self, x: 'Observer'):
        for i in self.subs:
            if i == x:
                self.subs.remove(i)

    def Fire(self, data):
        for z in self.subs:
            z.Notify(data)

class Observer:
    def Notify(self, data):
        pass

class Person(Observable):
    def __init__(self, age: int) -> None:
        super().__init__()
        self.__age = age

    @property
    def age(self) -> int:
        return self.__age

    @age.setter
    def age(self, age: int):
        if age == self.__age:
            return        
        self.__age = age
        self.Fire(PropertyChanged("age", self.__age))

class PropertyChanged:
    def __init__(self, Name: str, Value) -> None:
        self.Name = Name
        self.Value = Value

class TrafficManagement(Observer):
    def __init__(self, o: Observable) -> None:
        self.o = o

    def Notify(self, data: PropertyChanged):
        print("Notifying TrafficManagement")
        if data.Value >= 16:
            print("Congrats, you can drive now!")
            # we no longer care
            self.o.Unsubscribe(self)

def main():
    p = Person(15)
    t = TrafficManagement(p)
    p.Subscribe(t)
    
    for i in range(16, 21):
        print("Setting age to", i)
        p.age = i

main()
